<x-website-layout>
<x-page-banner-area :title="$service->title" :currentLocation="$service->title" />
<section class="services-details-area ptb-100">
	<div class="container">
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Coworking</h3>
						<p>We believe brand interaction is key in communication. Real innovations and a positive customer experience are the heart of successful communication. No fake products and services. The customer is king, their lives and needs are the inspiration.</p>
						<div class="features-text">
							<h4>Core Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
						</div>
						<div class="features-text">
							<h4>Define Your Choices</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="http://placehold.it/600x600" alt="image">
					</div>
				</div>
			</div>
		</div>
		<div class="services-details-overview mt-4">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="http://placehold.it/600x600" alt="image">
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Meeting Spaces</h3>
						<p>We believe brand interaction is key in communication. Real innovations and a positive customer experience are the heart of successful communication. No fake products and services. The customer is king, their lives and needs are the inspiration.</p>
						<div class="services-details-accordion">
							<div class="accordion">
								<div class="accordion-item">
									<div class="accordion-title active" href="javascript:void(0)">
										<i class='bx bx-plus'></i>
										Which material types can you work with?
									</div>
									<div class="accordion-content show">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
									</div>
								</div>
								<div class="accordion-item">
									<div class="accordion-title" href="javascript:void(0)">
										<i class='bx bx-plus'></i>
										Why Choose Our Services In Your Business?
									</div>
									<div class="accordion-content">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
									</div>
								</div>
								<div class="accordion-item">
									<div class="accordion-title" href="javascript:void(0)">
										<i class='bx bx-plus'></i>
										Does has Healthcare Mobile App
									</div>
									<div class="accordion-content">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</x-website-layout>