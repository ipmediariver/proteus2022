<x-website-layout>
    <x-website-slider />
    <x-website-services :services="$services" />

    <div class="subscribe-area ptb-100" style="background-image: url('http://placehold.it/1600x700');">
        <div class="container">
            <div class="subscribe-content-box">
                <div class="title">
                    <h2>Never Miss a Coworking Update</h2>
                    <p>Lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed diam voluptua.</p>
                </div>
                <form class="newsletter-form" data-bs-toggle="validator">
                    <div class="row">
                        <div class="col-lg-9 col-md-6">
                            <input type="email" class="input-newsletter" placeholder="Enter Your Email" name="EMAIL" required autocomplete="off">
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <button type="submit">Sign Me Up!</button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-website-layout>