<x-website-layout>
	<x-page-banner-area title="About us" currentLocation="About us" />
	<div class="benefits-area ptb-100">
	    <div class="container">
	        <div class="row align-items-center">
	            <div class="col-lg-7">
	                <div class="benefits-content">
	                    <span>About Us</span>
	                    <h3>Benefits to Setting Up Your Sustainable Startup in Our Coworking Creative Space</h3>
	                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin lorem quis bibendum auctor nisi elit consequat ipsum nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>
	                    <ul class="benefits-list">
	                        <li>
	                            <i class='bx bx-check'></i>
	                            Actual Office Space That Promoting Productivity
	                        </li>
	                        <li>
	                            <i class='bx bx-check'></i>
	                            Meaningful Connections With Your Team
	                        </li>
	                        <li>
	                            <i class='bx bx-check'></i>
	                            Increased Productivity To Get Some Work Done
	                        </li>
	                        <li>
	                            <i class='bx bx-check'></i>
	                            Actual Office Space That Promoting
	                        </li>
	                    </ul>
	                </div>
	            </div>
	            <div class="col-lg-5">
	                <div class="benefits-image">
	                    <img src="http://placehold.it/600x600" alt="image">
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="work-area">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-6">
	                <div class="work-image two" style="background-image: url('http://placehold.it/900x900')"></div>
	            </div>
	            <div class="col-lg-6">
	                <div class="work-content-item">
	                    <div class="content-box">
	                        <b>Give a Boost On Your Work</b>
	                        <h3>Team or Individuals Sustainable Coworking in Your Town</h3>
	                        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
	                        <div class="row justify-content-center">
	                            <div class="col-lg-4 col-md-4">
	                                <div class="work-fun-fact">
	                                    <h4>
	                                    <span class="odometer" data-count="3500">00</span>
	                                    <span class="sign-icon">m2</span>
	                                    </h4>
	                                    <p>Coworking Space</p>
	                                </div>
	                            </div>
	                            <div class="col-lg-4 col-md-4">
	                                <div class="work-fun-fact">
	                                    <h4>
	                                    <span class="odometer" data-count="1890">00</span>
	                                    <span class="sign-icon">People</span>
	                                    </h4>
	                                    <p>Office Amount</p>
	                                </div>
	                            </div>
	                            <div class="col-lg-4 col-md-4">
	                                <div class="work-fun-fact">
	                                    <h4>
	                                    <span class="odometer" data-count="426">00</span>
	                                    <span class="sign-icon">+</span>
	                                    </h4>
	                                    <p>Available Space Now</p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</x-website-layout>