<div>
    <x-website-header></x-website-header>
    {{ $slot }}
    <x-website-footer></x-website-footer>
</div>