<div class="top-header-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-9">
                <ul class="top-header-content">
                    <li>
                        <i class='bx bx-envelope'></i>
                        <a href="#"><span class="__cf_email__" data-cfemail="96e5e6e3e4f0d6f1fbf7fffab8f5f9fb">[info@proteusconsulting.com]</span></a>
                    </li>
                    <li>
                        <i class='bx bx-support'></i>
                        <a href="tel:012345678">(619) 739-8085</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-3">
                <ul class="top-header-optional">
                    <li>
                        <a href="https://www.facebook.com/" target="_blank">
                            <i class='bx bxl-facebook'></i>
                        </a>
                        <a href="https://twitter.com/?lang=en" target="_blank">
                            <i class='bx bxl-twitter'></i>
                        </a>
                        <a href="https://www.instagram.com/" target="_blank">
                            <i class='bx bxl-instagram-alt'></i>
                        </a>
                        <a href="https://www.google.com/" target="_blank">
                            <i class='bx bxl-google'></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>