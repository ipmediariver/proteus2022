<div class="navbar-area">
    <div class="main-responsive-nav">
        <div class="container">
            <div class="main-responsive-menu">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="/assets/images/logo.png" alt="logo">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="main-navbar">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="/assets/images/logo.png" alt="logo">
                </a>
                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a href="{{ route('home') }}" class="nav-link {{ \Route::is('home') ? 'active' : '' }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('about') }}" class="nav-link {{ \Route::is('about') ? 'active' : '' }}">About</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link {{ \Route::is('services.*') ? 'active' : '' }}">
                                Services
                                <i class="bx bx-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach(\App\Models\Service::get() as $service)
                                <li class="nav-item">
                                    <a href="{{ route('service.show', $service->slug) }}" class="nav-link">
                                        {{ $service->title }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('contact') }}" class="nav-link {{ \Route::is('contact') ? 'active' : '' }}">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Intranet</a>
                        </li>
                    </ul>
                    <div class="others-options d-flex align-items-center">
                        <div class="option-item">
                            <a href="#" class="default-btn">Fill our request form <i class='bx bx-plus'></i><span></span></a>
                        </div>
                        <div class="option-item">
                            <a href="{{ route('login') }}" class="optional-btn">Login <i class='bx bxs-user'></i><span></span></a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="others-option-for-responsive">
        <div class="container">
            <div class="dot-menu">
                <div class="inner">
                    <div class="circle circle-one"></div>
                    <div class="circle circle-two"></div>
                    <div class="circle circle-three"></div>
                </div>
            </div>
            <div class="container">
                <div class="option-inner">
                    <div class="others-options d-flex align-items-center">
                        <div class="option-item">
                            <a href="workspaces.html" class="default-btn">Add Your Space <i class='bx bx-plus'></i><span></span></a>
                        </div>
                        <div class="option-item">
                            <a href="login.html" class="optional-btn">Login <i class='bx bxs-user'></i><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>