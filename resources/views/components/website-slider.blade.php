<div class="main-slides-area">
    <div class="home-slides owl-carousel owl-theme">
        @foreach($slides as $slide)
        <div class="main-slides-item" style="background-image: url('{{ $slide['bgimg'] }}')">
            <div class="container">
                <div class="main-slides-content">
                    <p class="sub-title">{{ $slide['subtitle'] }}</p>
                    <h1>{{ $slide['title'] }}</h1>
                    <div class="slides-btn">
                        <a href="events-booking.html" class="default-btn">
                            Read more <i class='bx bxs-chevron-right'></i><span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>