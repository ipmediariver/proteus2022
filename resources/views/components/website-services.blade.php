<div class="solution-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Proteus Consulting</span>
            <h2>Binational Risk Management Specialist</h2>
        </div>
        <div class="row justify-content-center align-items-stretch">
            @foreach($services as $service)
            <div class="col-lg-4 col-md-6 mb-4">
                <x-service-card :service="$service" />
            </div>
            @endforeach
        </div>
    </div>
</div>