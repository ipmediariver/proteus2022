<div>
            <footer class="footer-area pt-100">
                <div class="container">
                    <div class="row d-flex">
                        <div class="col-lg-4 col-sm-6">
                            <div class="single-footer-widget">
                                <div class="widget-logo">
                                    <a href="index.html">
                                        <img src="/assets/images/logo.png" alt="image">
                                    </a>
                                </div>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin lorem quis bibendum auctor nisi elit consequat ipsum thnec sagittis sem nibh id elit.</p>
                                <ul class="widget-info mt-4">
                                    <li>
                                        <i class='bx bxs-phone'></i>
                                        <a href="tel:0023567890">(619) 739-8085</a>
                                    </li>
                                    <li>
                                        <i class='bx bx-envelope-open'></i>
                                        <a href="#"><span class="__cf_email__" data-cfemail="c7b4b7b2b5a187a0aaa6aeabe9a4a8aa">[info@proteusconsulting.com]</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="single-footer-widget">
                                <h3>Our Company</h3>
                                <ul class="footer-links-list">
                                    <li><a href="#">Our Location</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Events</a></li>
                                    <li><a href="#">News</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 ml-auto">
                            <div class="single-footer-widget">
                                <h3>Our Services</h3>
                                <ul class="footer-links-list">
                                    @foreach(\App\Models\Service::get() as $service)
                                    <li><a href="{{ $service->route }}">{{ $service->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright-area">
                    <div class="container">
                        <div class="copyright-area-content">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-6">
                                    <p>
                                        ©{{ date('Y') }} Proteus Consulting, Rights Reserved.
                                    </p>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <ul>
                                        <li>
                                            <a href="privacy-policy.html">Privacy Policy</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="go-top">
                <i class='bx bx-chevron-up'></i>
            </div>
            <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="/assets/js/jquery.min.js"></script>
            <script src="/assets/js/bootstrap.bundle.min.js"></script>
            <script src="/assets/js/jquery.meanmenu.js"></script>
            <script src="/assets/js/owl.carousel.min.js"></script>
            <script src="/assets/js/jquery.appear.js"></script>
            <script src="/assets/js/odometer.min.js"></script>
            <script src="/assets/js/nice-select.min.js"></script>
            <script src="/assets/js/jquery.magnific-popup.min.js"></script>
            <script src="/assets/js/jquery.ajaxchimp.min.js"></script>
            <script src="/assets/js/form-validator.min.js"></script>
            <script src="/assets/js/contact-form-script.js"></script>
            <script src="/assets/js/wow.min.js"></script>
            <script src="/assets/js/main.js"></script>
        </body>
    </html>
</div>