<div class="page-banner-area" style="background-image: url('http://placehold.it/1600x700')">
    <div class="container">
        <div class="page-banner-content">
            <h2>{{ $title }}</h2>
            <ul class="pages-list">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li>{{ $currentLocation }}</li>
            </ul>
        </div>
    </div>
</div>