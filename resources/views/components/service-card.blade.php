<div class="single-solution">
    <div class="solution-image">
        <a href="{{ $service->route }}">
            <img src="{{ $service->thumb }}" alt="{{ $service->title }}">
        </a>
    </div>
    <div class="solution-content">
        <h3>
        <a href="services-details.html">{{ $service->title }}</a>
        </h3>
        @foreach(json_decode($service->specs) as $spec)
        <p>{{ $spec }}</p>
        @endforeach
    </div>
</div>