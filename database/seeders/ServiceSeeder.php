<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::factory()->create([
            'title'    => 'Executive secure transportation',
            'subtitle' => 'We provide turnkey secure transportation solutions for your single VIP or group of VIP\'s.',
            'specs'    => json_encode(['One time services', 'Extended time contracts', 'Sentri holding drivers available']),
            'slug'     => \Str::slug('Executive secure transportation'),
        ]);

        Service::factory()->create([
            'title'    => 'Executive protection and security details',
            'subtitle' => 'We specialize in protecting groups and individuals from multinational companies in Mexico.',
            'specs'    => json_encode(['Personal security details', 'Cross border teams available', 'Armored vehicle rentals']),
            'slug'     => \Str::slug('Executive protection and security details'),
        ]);

        Service::factory()->create([
            'title'    => 'Consulting',
            'subtitle' => 'The most effective security programs are those that are based on preventive strategies and infrastructures, rather than waiting for the problem to occur and then taking corrective measures.',
            'specs'    => json_encode([
                'Crime Intelligence Mapping (MEXICO)',
                'Risk/Threat Assessment',
                'Complete Security Programs',
                'CSO Outsourcing',
                'Augmented Due Diligence',
                'Loss Prevention Programs',
                'Augmented CCTV & Access Control',
                'Disaster Recovery/ EVAC Programs',
                'Medical Emergency & Contingency',
            ]),
            'slug'     => \Str::slug('Consulting'),
        ]);

        Service::factory()->create([
            'title'    => 'Invesigative services and background checks',
            'subtitle' => 'Do you know where your employees have been? we have an advantage when presenting investigation and security options for clients being able to see their entire security profile.',
            'specs'    => json_encode(['US/Mexico background check', 'Coporate investigations', 'Employee trustworthiness']),
            'slug'     => \Str::slug('Invesigative services and background checks'),
        ]);

        Service::factory()->create([
            'title'    => 'Security training',
            'subtitle' => 'Counter kidnap and crime avoidance training, defensive and evasive driving course.',
            'specs'    => json_encode([
                'Tactical Armed & Un-armed Training', 
                'Whole Family Contingency Courses', 
                'Personal Security Details Certification',
                'Tactical Evasive/Defensive Driver Training',
                'EMTR (Emergency Medical Tactical Response)',
                'Crime Prevention and Awareness Courses'
            ]),
            'slug'     => \Str::slug('Security training'),
        ]);

        Service::factory()->create([
            'title'    => 'Geo location of assets and persons',
            'subtitle' => 'We keep discrete secure tracking of you while on travel. We are ready to respond in case of an emergency. We locate you and extract you wherever you are. In case of natural disaster we will come and rescue you.',
            'specs'    => json_encode(['Secure travel logistics', 'Travel following and crisis response', 'Emergency rescue and medical evacuation']),
            'slug'     => \Str::slug('Geo location of assets and persons'),
        ]);

        Service::factory()->create([
            'title'    => 'Corporate security programs for companies doing business in mexico',
            'subtitle' => 'We tailor security programs for our customers, based on their specific threat and risk atmospherics.',
            'specs'    => '',
            'slug'     => \Str::slug('Corporate security programs for companies doing business in mexico'),
        ]);
    }

}
