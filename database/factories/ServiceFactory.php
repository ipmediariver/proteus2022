<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $specs = [$this->faker->realText(40), $this->faker->realText(40), $this->faker->realText(40)];
        $specs = json_encode($specs);
        $title = $this->faker->sentence();
        return [
            'title'    => $title,
            'subtitle' => $this->faker->realText(100),
            'specs'    => $specs,
            'cover'    => 'http://placehold.it/1600x750',
            'thumb'    => 'http://placehold.it/600x300',
            'body'     => $this->faker->randomHtml(2, 3),
            'slug'     => \Str::slug($title)
        ];
    }
}
