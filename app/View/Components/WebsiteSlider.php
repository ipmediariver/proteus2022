<?php

namespace App\View\Components;

use Illuminate\View\Component;

class WebsiteSlider extends Component
{


    public $slides = [
        [
            'id' => 1,
            'subtitle' => 'We provide turnkey secure transportation solutions for your single VIP or group of VIP\'s.',
            'title' => 'Executive Secure Transportation',
            'bgimg' => '/assets/slides/executive-secure-transportation.jpg'
        ],
        [
            'id' => 2,
            'subtitle' => 'We specialize in protecting groups and individuals from multinational companies in Mexico.',
            'title' => 'Executive Protection and Security Details',
            'bgimg' => '/assets/slides/executive-protection-and-security-details.jpg'
        ],
        [
            'id' => 3,
            'subtitle' => 'We tailor security programs for our customers, based on their specific threat and risk atmospherics.',
            'title' => 'Corporate Security Programs For Companies Doing Business In Mexico',
            'bgimg' => '/assets/slides/executive-protection-and-security-details.jpg'
        ]
    ];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.website-slider');
    }
}
