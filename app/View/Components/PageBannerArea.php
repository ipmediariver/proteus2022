<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PageBannerArea extends Component
{

    public $title;
    public $currentLocation;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $currentLocation)
    {
        $this->title = $title;
        $this->currentLocation = $currentLocation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.page-banner-area');
    }
}
