<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Service extends Model
{
    use HasFactory;
    protected $appends = ['route'];
    public function getRouteAttribute(){
        return route('service.show', $this->slug);
    }
}