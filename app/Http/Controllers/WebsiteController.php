<?php
namespace App\Http\Controllers;

use App\Models\Service;
class WebsiteController extends Controller
{
    public function index()
    {
        $services = Service::all()->makeVisible('route');
        return view('welcome', compact('services'));
    }
    public function service(Service $service){
        return view('service', compact('service'));
    }
    public function about()
    {
        return view('about');
    }
    public function contact()
    {
        return view('contact');
    }
}